/**Author: Kevin Echenique Arroyo
 * Student #: 1441258
 */
public class Bicycle{
    
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

//Constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

//Get methods
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

//toString method
    public String toString(){
        return ("Manufacturer: " + this.manufacturer + "\n" + "Number of Gears: " + this.numberGears + "\n" + "Max Speed: " + this.maxSpeed + "\n"+"\n");
    }

}