/**Author: Kevin Echenique Arroyo
 * Student #: 1441258
 */
public class BikeStore{
    public static void main(String[] args){
        
        Bicycle[] bikeCollection= new Bicycle[4];

        bikeCollection[0] = new Bicycle("HelloBikes", 4, 20.5);
        bikeCollection[1] = new Bicycle("TomorrowWheels", 10, 54.6);
        bikeCollection[2] = new Bicycle("Sportify", 20, 80.3);
        bikeCollection[3] = new Bicycle("IsThisEvenABike", 100, 580.5);

        for(int i=0; i<bikeCollection.length; i++){
            System.out.println(bikeCollection[i]);
        }

    }
}